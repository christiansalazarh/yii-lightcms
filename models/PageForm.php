<?php
class PageForm extends CFormModel
{
	public $name;
	public $title;
	public $subtext;
	public $col1;
	public $col2;
	public $col3;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array('name, title, subtext, col1,col2,col3', 'safe'),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'name'=>'Pagina',
			'title'=>'Titulo Principal',
			'subtext'=>'Texto Principal',
			'col1'=>'Columna 1',
			'col2'=>'Columna 2',
			'col3'=>'Columna 3',
		);
	}

	public function encode(){
		return json_encode($this->attributes);
	}
}
