<p>
	Se dispone de un grupo predefinido de paginas. Elija una:
</p>
<ul id='lightcms-pages'>
<?php 
	foreach($available as $page){
		$label = ucwords($page);
		$u = CHtml::normalizeUrl(array('updatepage','name'=>$page));
		echo "<li><a href='$u' class='alert' >$label</a></li>";
	}
?>
</ul>
