<?php 
	$action = CHtml::normalizeUrl(array('admin/galleryset'));
?>

<p>Permite asociar las colecciones disponibles a las galerias predefinidas del sistema.</p>

<form action='<?=$action;?>' method='post'>
	<?php
		foreach($list as $gallerie=>$collection){
			$info = $this->moduleProperty("galleries")[$gallerie];
			$s=(""==$collection) ? "selected" : "";
			$options = "<option value='ninguna' $s>Ninguna</option>";
			foreach($collections as $collection_id=>$name){
				$s=($collection_id==$collection) ? "selected" : "";
				$options .= "<option value='$collection_id' $s>$name</option>";
			}
			echo "<fieldset style='display: inline-block; padding: 10pt;'>";
				echo "<label>$info</label>";
				echo "<select name='$gallerie' class='form-control'>$options</select>";
			echo "</fieldset>";
		}
	?>
	<div class='buttons'>
		<button type='submit' name='submit' class='btn btn-lg btn-primary'>Guardar</button>
	</div>
</form>
