<?php
class CollectionForm extends CFormModelSanitized {
	public $id;
	public $title;
	public function rules(){
		return array(
			array('id','required','on'=>'update'),
			array('title','required'),
		);
	}
	public function attributeLabels()
	{
		return array(
			'title'=>'Nombre de la Colección',
		);
	}
	
}
