<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'updatepage-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<fieldset>
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',
			array('class'=>'form-control',
				'placeholder'=>'')); ?>
	</fieldset>

	<fieldset>
		<?php echo $form->labelEx($model,'subtext'); ?>
		<?php echo $form->textArea($model,'subtext',
			array('class'=>'form-control',
				'placeholder'=>'')); ?>
	</fieldset>
	
	<div class='row'>
		<div class='col-md-4'>
			<fieldset>
				<?php echo $form->labelEx($model,'col1'); ?>
				<?php echo $form->textArea($model,'col1',
					array('class'=>'form-control w5','rows'=>5,
						'placeholder'=>'')); ?>
			</fieldset>
		</div>
		<div class='col-md-4'>
			<fieldset>
				<?php echo $form->labelEx($model,'col2'); ?>
				<?php echo $form->textArea($model,'col2',
					array('class'=>'form-control w5','rows'=>5,
						'placeholder'=>'')); ?>
			</fieldset>
		</div>
		<div class='col-md-4'>
			<fieldset>
				<?php echo $form->labelEx($model,'col3'); ?>
				<?php echo $form->textArea($model,'col3',
					array('class'=>'form-control w5','rows'=>5,
						'placeholder'=>'')); ?>
			</fieldset>
		</div>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Guardar',
			array('class'=>'btn btn-lg btn-primary')); ?>
		<a class='btn btn-lg btn-warning' 
			href='<?=CHtml::normalizeUrl(array('pages'));?>'>Volver</a>
	</div>


<?php $this->endWidget(); ?>
