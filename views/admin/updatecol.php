<?php
	$identity = "gallery-".$model->id;
	// readed into MyYiiFileManViewer
	Yii::app()->user->setState("selected_identity", $identity);
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'updatecol-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Guardar Coleccion',
			array('class'=>'btn btn-lg btn-primary')); ?>
	</div>
<hr/>

<?php $this->endWidget(); ?>
<div class='row panel panel-info'>
	<div class='panel-heading'>Imagenes de la Colección</div>
	<div class='panel-body'>
		<p>Aqui se suben las imagenes correspondientes a esta colección.</p>
		<div class='gallery col-md-12'>
			<div id='file-picker-viewer'>
				<div class='body col-md-12'></div>
				
				<div id='myuploader' class='col-md-12'>
					<label>Selector de Archivos</label>
					<p>Uselo para subir nuevas fotos a la galeria</p>
					<div rel='pin' class=''></div>
					<br/>
					<div class='files'></div>
					<div class='progressbar'>
						<div style='float: left;'>Subiendo archivos...</div>
						<img style='float: left;' src='images/progressbar.gif' />
						<div style='float: left; margin-right: 10px;'class='progress'></div>
						<img style='float: left;' class='canceljob' 
							src='images/delete.png' title='click para cancelar subida'/>
					</div>
				</div>
				<div class='buttons'>
					<button id='delete_file' 
						class='delete_button btn btn-warning'>Eliminar Fotos Seleccionadas</button>
				</div>
			</div>
			<div id='logger'></div>
			<?php
				// the widget
				//
				$this->widget('cms.components.MyYiiFileManViewer'
				,array(
					// layout selectors:
					'launch_selector'=>null,
					'list_selector'=>'#file-picker-viewer',
					'uploader_selector' => '#myuploader',
					// messages:
					'delete_confirm_message' => 'Confirm deletion ?',
					'select_confirm_message' => 'Confirm selected items ?',
					'no_selection_message' => 'You are required to select some file',
					// events:
					'onBeforeAction'=>"function(viewer,action,file_ids) { return true; }",
					'onAfterAction'=>"function(viewer,action,file_ids, ok, response) { 
						if(action == 'select'){ // actions: select | delete
							$.each(file_ids, function(i, item){ 
								$('#logger').append('file_id='+item.file_id 
									+ ', <img src=\''+item.url+'&size=full\'><br/>');
							});
						}
					}",
					// 'onBeforeLaunch'=>"function(_viewer){ }",
					'onClientSideUploaderError'=>
						"function(messages){ 
							$(messages).each(function(i,m){ 
								alert(m); 
							}); 
						}
					",
					'onClientUploaderProgress'=>"function(status, progress){
						// $('#logger').append('progress: '+status+' '+progress+'%<br/>');
					}",
				));
			?>
		</div>
	</div>
</div>

<i style='font-family: courier;font-size: smaller;'><?=$model->id;?></i>

<?php
Yii::app()->getClientScript()->registerScript(
"upload_button_class_modified",
"
	$('.files input[name=submit]').addClass('btn btn-primary btn-lg upload-btn');
	$('.files input[name=submit]').val('Subir Archivos');
",CClientScript::POS_LOAD);
?>
