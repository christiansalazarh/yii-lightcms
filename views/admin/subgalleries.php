<?php
	// $list
	$html = "";
	foreach($list as $id=>$label){
		$url = CHtml::normalizeUrl(array('setgallerie','id'=>$id));
		$html .= "
		<div class='col-md-12 listrow'>
			<div class='col-md-8'>
				<a href='$url'><span class='fa fa-pencil'></span>
					<span class='font'>{$label}</span></a>
			</div>
		</div>";
	}
?>
<p>Las galerias secundarias aparecen al hacer click en una foto en el website. 
Haga click en un archivo para crearle una galeria secundaria.</p>
<?=$html;?>


