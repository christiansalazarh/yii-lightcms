<?php
class CFormModelSanitized extends CFormModel {
	public function setAttributes($values, $safeOnly=true){
		parent::setAttributes(
			filter_var_array($values, FILTER_SANITIZE_STRING), $safeOnly);
	}
}
