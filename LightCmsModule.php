<?php
class LightCmsModule extends CWebModule {
	public $defaultController='admin';
	public $fileapi='fileman';  // instance of YiiDiskFileManager
								// use 'fileman' for Yii::app()->fileman
	
	public $galleries;
	public $pages;
	public $gallery_url_pattern;
	/* examples 
		'gallery_url_pattern'=>'http:/ate.local/gallery/picture/%data%', 
		'galleries'=>array(                                              
			'home'=>'Carousel (940x540px)',                              
			'home-sidebar'=>'Sidebar (375x200px)',                       
			'home-services'=>'Servicios (200x200px)',                    
			'clientes'=>'Clientes (100x100)',                            
		),                                                               
		'pages'=>array(                                                  
			'empresa',                                                   
			'productos',                                                 
		),                                                               
	 */

	public function init(){
		$this->setImport(array(
			// "cms" is defined in protected/config/main.php in modules array
			'cms.components.*',
			'cms.models.*',
			'cms.controllers.*',
		));
	}
	public function beforeControllerAction($controller, $action){
		if(parent::beforeControllerAction($controller, $action)){
			return true;
		}else
		return false;
	}
}
