<?php
/*
	"collections" {identity}
		|
		--->collection_1 {file_id_1}	{title: xxx123}
		--->collection_2 {file_id_2}
		--->collectio... {file_id..}
		--->collection_n {file_id_n}

	"gallery-{file_id_1}"
		|
		--->picture_1	{file_id_A}
		--->picture_2	{file_id_B}
		--->picture_3	{file_id_C} ..
	
 */
class CollectionApi extends CApplicationComponent {
	
	private function moduleProperty($propertyName){
		return Yii::app()->getModule('cms')->$propertyName;
	}
	
	private function getFapi(){
		$fapi = $this->moduleProperty('fileapi');
		return Yii::app()->$fapi;
	}
	
	private function writeTmp($data){
		$tmp = tempnam("/tmp", "gys");
		$f = fopen($tmp, "w");
		$d = serialize($data);
		fwrite($f, $d, strlen($d));
		fflush($f);
		fclose($f);
		return $tmp;
	}

	public function create($title){
		$fapi = $this->getFapi();
		$identity = "collections";
		$tmp = $this->writeTmp(array(
			"title"=>$title,
		));
		list($file_id) = $fapi->add_files($identity, $tmp);
		return $file_id;
	}

	public function updatecol($file_id, $title){
		$fapi = $this->getFapi();
		$identity = "collections";
		$local_path = $fapi->get_file_path($identity, $file_id);
		$f = fopen($local_path, "w");
		$data = serialize(array("title"=>$title));
		fwrite($f, $data, strlen($data));
		fclose($f);
	}

	public function getlist(){
		$fapi = $this->getFapi();
		$identity = "collections";
		$list = array();
		foreach($fapi->list_files($identity) as $fd){
			$file_id = $fd['file_id'];
			$local_path = $fapi->get_file_path($identity, $file_id);
			$data = unserialize(file_get_contents($local_path));
			$list[$file_id] = $data;
		}
		return $list;
	}

	public function getcol($id){
		$fapi = $this->getFapi();
		$identity = "collections";
		foreach($fapi->list_files($identity) as $fd){
			$file_id = $fd['file_id'];
			if($file_id == $id){
				$local_path = $fapi->get_file_path($identity, $file_id);
				$data = unserialize(file_get_contents($local_path));
				return array("id"=>$file_id, "title"=>$data['title']);
			}
		}
		return null;
	}

	public function getlistdataprovider(){
		$list=array();
		foreach($this->getlist() as $file_id=>$data)
			$list[] = array("id"=>$file_id,"title"=>$data["title"]);
		return new CArrayDataProvider($list, array(
			'keyField' => 'id',
		));
	}

	public function getlistoptions(){
		$list=array();
		foreach($this->getlist() as $file_id=>$data)
			$list[$file_id] = $data["title"];
		return $list;
	}

	public function delete($id){
		$fapi = $this->getFapi();
		$identity = "collections";
		$fapi->remove_files($identity, $id);
	}

	public function loadModel($id){
		if(!$item = $this->getcol($id)) return null;
		$model = new CollectionForm('update');
		$model->id = $item['id'];
		$model->title = $item['title'];
		return $model;
	}

	public function enumGalleryFiles($collection_id,$boolAddInfo=false){
		$fapi = $this->getFapi();
		$identity = "gallery-{$collection_id}";
		$files = array();
		foreach($fapi->list_files($identity) as $fd){
			$file_id = $fd['file_id'];
			$local_path = $fapi->get_file_path($identity, $file_id);
			if('info' == $fd['filename'] && false==$boolAddInfo) continue;
			if('info' == $fd['filename'] && true==$boolAddInfo){
				$files[$file_id] = unserialize(@file_get_contents($local_path));
				continue;
			}
			$files[$file_id] = $local_path;
		}
		return $files;
	}

	public function enumGalleryFilesExtended($collection_id){
		$fapi = $this->getFapi();
		$identity = "gallery-{$collection_id}";
		$files = array();
		foreach($fapi->list_files($identity) as $fd){
			if('info' == $fd['filename']) continue;
			$file_id = $fd['file_id'];
			$local_path = $fapi->get_file_path($identity, $file_id);
			$files[$file_id] = array($local_path, $fd);
		}
		return $files;
	}

	public function getCollectionFile($collection, $file_id){
		$fapi = $this->getFapi();
		$identity = "gallery-{$collection}";
		return $fapi->get_file_info($identity, $file_id);
	}

	public function encodeUrl($collection, $file_id){
		$id = base64_encode(serialize(array($collection,$file_id)));
		$url = str_replace("%data%",$id,$this->moduleProperty('gallery_url_pattern'));
		return $url;
	}

	public function enumGalleryFilesHtml($collection_id){
		$html = "<ul class='collection'>\n";
		$files = $this->enumGalleryFilesExtended($collection_id);
		foreach($files as $file_id=>$data){
			list($local_path, $info) = $data;
			$subgallery_identity = "gallery-{$collection_id}-{$file_id}";
			$url = $this->encodeUrl($collection_id, $file_id);
			$img_url = "/media/+/+/".base64_encode($subgallery_identity);
			$img_tag = "<img src='$img_url' 
							style='max-width: 100px;' class='thumb' />";
			$_html = "\t<li>$img_tag
				<label>{$info['filename']}</label></li>\n";
			// detect if it has sub galleries, so provide a link to access it.
			$ginfo = $this->getGalleryInfo($subgallery_identity);
			if(0 !== count($ginfo)){
				$s = str_replace(array(' ','%','?','#','<','>'),
					"+",strtolower($info['filename']));
				$baseurl = "/$s/{$collection_id}-{$file_id}";
				$_html = 
				"\t<li>
						<a href='$url'>
						$img_tag
						<label>{$info['filename']}</label></a>
					</li>\n";
			}	
			$html .= $_html;
		}
		$html .= "</ul>\n";
		return $html;
	}

	public function enumGalleryFilesCarousel($collection_id){
		$files = $this->enumGalleryFilesExtended($collection_id);
		$inner = "<div class='carousel-inner' role='listbox'>";
		$active = "active";
		foreach($files as $file_id=>$data){
			list($local_path, $info) = $data;
			$id = base64_encode(serialize(array($collection_id,$file_id)));
			$label = ucwords($info['filename']);
			$img_src = str_replace("%data%",$id,$this->moduleProperty('gallery-url'));
			$inner .= 
			"
				<div class='item $active'>
					<img src='$img_src'>
					<div class='carousel-caption'>$label</div>
				</div>
			";	
			$active = "";
		}
		$inner .= "</div>\n";
		return $inner;
	}

	public function getFileContent($encoded_id){
		$data = unserialize(base64_decode($encoded_id));
		if(!is_array($data))
			return null;
		list($collection_id, $file_id) = $data;
		$fapi = $this->getFapi();
		$identity = "gallery-{$collection_id}";
		$local_path = $fapi->get_file_path($identity, $file_id);
		return $local_path;
	}

	public function getAttachedCollection($gallery){
		$fapi = $this->getFapi();
		$setup_file_name = rtrim($fapi->storage_path,"/")."/galleries.json";
		if(!$data = json_decode(@file_get_contents($setup_file_name),true))
			return null;
		if(isset($data[$gallery])){
			return $data[$gallery];
		}else
		return null;
	}

	public function setAttachedCollection($gallery, $collection_id){
		$fapi = $this->getFapi();
		$setup_file_name = rtrim($fapi->storage_path,"/")."/galleries.json";
		if(!$data = json_decode(@file_get_contents($setup_file_name),true))
			$data = array();
		$data[$gallery] = $collection_id;
		$f = fopen($setup_file_name,"w");
		fprintf($f,"%s",json_encode($data));
		fclose($f);
	}

	public function listGalleries(){
		// see also:  
		//	./yiic test testcollection --mode=galleries
		//	./yiic test testcollection --mode=set --d=home:4ddba878
		$galleries = $this->moduleProperty('galleries');
		$list = array();
		foreach($galleries as $gallery=>$info){
			$list[$gallery] = $this->getAttachedCollection($gallery);
		}
		return $list;
	}

	public function getGallery($gallery){
		if(!$collection = $this->getAttachedCollection($gallery))
			return null;
		return $this->enumGalleryFiles($collection);
	}

	public function getGalleryHtml($gallery){
		if(!$collection = $this->getAttachedCollection($gallery))
			return "";
		return $this->enumGalleryFilesHtml($collection);				
	}
	
	public function getGalleryCarousel($gallery){
		if(!$collection = $this->getAttachedCollection($gallery))
			return "";
		return $this->enumGalleryFilesCarousel($collection);				
	}

	public function listAllFiles(){
		// returns all the files associated to collections
		$collections = $this->getlist(); // all the collections available
		$result = array();
		foreach($collections as $collection=>$c){
			foreach($files = $this->enumGalleryFilesExtended($collection) 
				as $file_id=>$data){
				list($local_path, $info) = $data;
				$id = "{$collection}-{$file_id}";
				$result[$id] = $c['title']."->".$info['filename'];
			}
		}
		return $result;
	}

	public function getGalleryInfo($identity){
		$fapi = $this->getFapi();	
		foreach($fapi->list_files($identity) as $fd){
			if("info" == $fd['filename']){
				// found.
				$local_path = $fapi->get_file_path($identity, $fd['file_id']);
				return unserialize(@file_get_contents($local_path));
			}
		}
		return array();	
	}

	public function setGalleryInfo($identity, $model){
		$fapi = $this->getFapi();	
		foreach($fapi->list_files($identity) as $fd){
			if("info" == $fd['filename']){
				$fapi->remove_files($identity, $fd['file_id']);
			}
		}
		$tmp = $this->writeTmp($model->attributes);
		list($file_id) = $fapi->add_files($identity, $tmp);
		$fapi->rename_file($identity, $file_id, "info");
	}

	public function savePage($name, $data){
		$fapi = $this->getFapi();
		$file = rtrim($fapi->storage_path,"/")."/page-$name.json";
		file_put_contents($file, $data);
	}

	public function loadPage($name){
		$fapi = $this->getFapi();
		$file = rtrim($fapi->storage_path,"/")."/page-$name.json";
		if($data = json_decode(@file_get_contents($file),true)){
			return $data;
		}else
		return array('name'=>$name,'title'=>ucwords($name),
			'subtext'=>'Esta pagina no tiene texto secundario, hay que crearle uno.',
			'col1'=>'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos',
			'col2'=>'No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.',
			'col3'=>'Al contrario del pensamiento popular, el texto de Lorem Ipsum no es simplemente texto aleatorio. Tiene sus raices en una pieza cl´sica de la literatura del Latin, que data del año 45 antes de Cristo, haciendo que este adquiera mas de 2000 años de antiguedad.'
		);
	}

	public function expandMacros($entries,$classname=''){
		$d = array();
		foreach($entries as $attr=>$value){
			if(preg_match_all('(\@([a-z0-9]+)\@)',$value,$m)){
				//$value = "<pre>".print_r($m,true)."</pre>";
				$keys = array();
				$data = array();
				foreach($m[1] as $key){
					$keys[] = "@".$key."@";
					$data[] = "<div class='wrapped-gallery $classname'>"
						.$this->enumGalleryFilesHtml($key)."</div>";
				}
				$value = str_replace($keys, $data, $value);
			}
			$d[$attr] = $value;
		}
		return $d;
	}

	public function listImages(){
		$r = array();
		if($col_list = $this->getlist()){
			foreach($col_list as $collectionid=>$data){
				$title = $data['title'];
				$safetitle = str_replace(array('#','?','<','>',' '),'+',strtolower($title));
				if($files = $this->enumGalleryFilesExtended($collectionid)){
					foreach($files as $file){
						list($path, $fd) = $file;
						$title2 = $fd['filename'];
						$safetitle2 = str_replace(array('#','?','<','>',' '),
							'+',strtolower($title2));
						$safetitle2 = str_replace(array(
							"++",".png",".jpg",".jpeg"
							),"+",$safetitle2);
						$_id = base64_encode($fd['id']."-".$fd['file_id']);
						$url = "/media/".$safetitle."/".$safetitle2."/".$_id;
						$url = str_replace(array("++"),"",$url);
						$url = str_replace(array("+/"),"/",$url);
						$r[] = array($fd['id'],$title,$title2,$url);
					}
				}
			}
		}
		return $r;
	}
}

