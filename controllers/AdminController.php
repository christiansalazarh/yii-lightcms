<?php

class AdminController extends CController
{
	public $layout = "admin";
	public $title;
	public $menu;
	public $defaultAction = 'index';

	public function init(){
		$this->title = "Panel de Control";
		$this->menu = $this->getMainMenu();
		$this->prepareAssets();
	}

	public function moduleProperty($propertyName){
		return Yii::app()->getModule('cms')->$propertyName;
	}

	public function filters(){
		return array(
			'accessControl'
		);
	}

	public function accessRules(){
		return array(
			array('deny','users'=>array('?')),
			array('allow','users'=>array('*')),
		);
	}

	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			'filemanp'=>
				array('class'=>
					'YiiFileManagerFilePickerAction'),
		);
	}

	public function prepareAssets(){
		$localdir = rtrim(dirname(__FILE__),"/")."/../assets/";
		$assets = rtrim(Yii::app()->getAssetManager()->publish($localdir),"/");
		$cs = Yii::app()->getClientScript();
		$cs->registerCoreScript('jquery');
		$cs->registerCssFile($assets."/lightcms-admin.css");
		// $cs->registerScriptFile($assets."/bla.js");
	}

	private function getMainMenu(){
		return array(
			array('label'=>'Colecciones','url'=>array('listcol')),
			array('label'=>'Galerias','url'=>array('gallerycol')),
			array('label'=>'Galerias Secundarias','url'=>array('subgalleries')),
			array('label'=>'Editar Paginas','url'=>array('pages')),
			array('label'=>'Imagenes','url'=>array('imagelist')),
			array('label'=>'Salir','url'=>array('logout')),
		);
	}	

	public function actionIndex()
	{
		$this->title = "Bienvenido";
		$this->menu = $this->getMainMenu();
		$this->render('index');
	}

	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionMsg($d){
		$this->title = "Mensaje de Sistema";
		$this->renderText(unserialize($d));
	}

	public function actionCreatecol(){
		$this->title = "Crear Nueva Coleccion";
		$model = new CollectionForm;
		if(isset($_POST['CollectionForm'])){
			$model->attributes = $_POST['CollectionForm'];
			if($model->validate()){
				$api = new CollectionApi;
				$file_id = $api->create($model->title);
				$editme = CHtml::link("Editar la Colección",
					array('updatecol','id'=>$file_id),
						array('class'=>'btn btn-primary'));
				$this->redirect(array('/cms/admin/msg',
					'd'=>serialize('Coleccion Creada.<hr/>'.$editme)));
			}
		}
		$this->render('createcol',array('model'=>$model));
	}

	public function actionListcol(){
		$this->title = "Colecciones";
		$api = new CollectionApi;
		$dp = $api->getlistdataprovider();
		$this->render('listcol',array('dataProvider'=>$dp));
	}

	public function actionDeletecol($id){
		$api = new CollectionApi;
		$api->delete($id);
		$this->title = "Coleccion Eliminada";
		$this->renderText("La coleccion ha sido eliminada.");
	}

	public function actionUpdatecol($id){
		$api = new CollectionApi;
		if(!$model = $api->loadModel($id))
			throw new Exception("El elemento seleccionado ya no existe.");
		$this->title = "Editando Coleccion: <b>{$model->title}</b>";
		if(isset($_POST['CollectionForm'])){
			$model->attributes = $_POST['CollectionForm'];
			if($model->validate()){
				$api->updatecol($model->id, $model->title);
				$this->redirect(array('/cms/admin/msg',
					'd'=>serialize('Coleccion Actualizada')));
			}
		}
		$this->render('updatecol',array('model'=>$model));
	}

	public function actionConfirmDelete($ok, $cancel){
		$this->title = "Confirme la Eliminación";
		$h1 = CHtml::normalizeUrl(unserialize($ok));
		$h2 = CHtml::normalizeUrl(unserialize($cancel));
		$this->renderText(
			"
				<div class='hbut'>
					<a class='btn btn-lg btn-danger' href='$h1'>Confirma Eliminar</a>
					<a class='btn btn-lg btn-primary' href='$h2'>Cancelar</a>
				</div>
			");
	}

	public function actionGalleryCol(){
		$this->title = "Galerias y Colecciones";
		$api = new CollectionApi;
		$list = $api->listGalleries();
		$collections = $api->getlistoptions();

		$this->render('gallerycol',
			array("list"=>$list,"collections"=>$collections));
	}

	public function actionGallerySet(){
		$this->title = "Asocia Colecciones a Galerias.";
		$api = new CollectionApi;
		$list = $api->listGalleries();
		//$d = "<pre>".print_r(array($list,$_POST),true)."</pre>";
		$d="";
		foreach($_POST as $gallerie=>$collection_id){
			if("submit"===$gallerie) continue;
			$api->setAttachedCollection($gallerie, 
				"ninguna"==$collection_id ? null:$collection_id);
		}
		$this->renderText("Cambios Realizados.".$d);
	}

	public function actionSubgalleries(){
		$this->title = "Galerias Secundarias";
		$api = new CollectionApi;
		$list = $api->listAllFiles();
		$list2 = array();
		foreach($list as $id=>$label){
			$c = "";
			$files = $api->enumGalleryFilesExtended($id);
			$n = count($files);
			$c = $n ? " ($n)" : "";
			$list2[$id] = $label.$c;
		}

		$this->render('subgalleries',array('list'=>$list2));
	}

	public function actionSetgallerie($id){
		$_id = explode("-",$id);
		$collection = $_id[0];
		$file_id = $_id[1];
		$api = new CollectionApi;
		$filename = $api->getcol($collection)['title']
			."->".$api->getCollectionFile($collection, $file_id)['filename'];
		$identity = "gallery-".$id;

		$this->title = "Editando ".$filename."";

		$model = new FileForm;
		$model->id = $identity;
		$model->attributes = $api->getGalleryInfo($identity);
		if(isset($_POST['FileForm'])){
			$model->attributes = $_POST['FileForm'];
			if($model->validate()){
				$api->setGalleryInfo($identity, $model);
			}
		}

		$this->render('setgallerie',
			array('identity'=>$identity,'model'=>$model));
	}

	public function actionPages(){
		$this->title = "Edicion de Páginas";
		$api = new CollectionApi;
	
		$this->render('pages',
			array('available'=>$this->moduleProperty('pages')));
	}

	public function actionUpdatePage($name){
		$this->title = 'Editando Pagina: '.ucwords($name);
		$model = new PageForm;
		$api = new CollectionApi;
		$model->name = $name;
		$model->attributes = $api->loadPage($name);
		if(isset($_POST['PageForm'])){
			$model->attributes = $_POST['PageForm'];
			$api->savePage($name, $model->encode());
			//$this->redirect(array('pages'));
		}

		/*
		Yii::app()->getClientScript()->registerScript(
		"wysihtml5_script_id",
		"
			console.log('wysihtml5 script loaded');
			$('.w5').wysihtml5();
		",CClientScript::POS_LOAD);
		*/
		
		$this->render('updatepage',array('model'=>$model));
	}

	public function actionImagelist(){
		$this->title = "Lista de Imagenes";
		$api = new CollectionApi;
		$list = $api->listImages();
		$this->render('imagelist',array('list'=>$list));
	}

}
