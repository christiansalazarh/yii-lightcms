<!DOCTYPE html>
<html>
<head>
	<link rel="SHORTCUT ICON" href="images/favicon.png">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" /> 
	<meta name="language" content="en">
	<title>Control Panel</title>
</head>
<body>
	<div class='row main font'>
		<h1 class='main'>Control Panel</h1>
		<div class='col-md-3'>
			<div class='panel panel-primary'>
				<div class='panel-heading'>Operations</div>
				<div class='panel-body'>
					<?php                                                	
						$this->widget('zii.widgets.CMenu', array(
							'items'=>$this->menu,
							'htmlOptions'=>array('class'=>'sidebar'),
							'itemTemplate'=>'<span class=\'fa fa-plus\'></span>{menu}',
						));
					?>	
				</div>
			</div>
		</div>
		<div class='col-md-9'>
		<div class='panel panel-primary'>
			<div class='panel-heading'><?=$this->title;?></div>
			<div class='panel-body'>
				<div class='row'>
					<div class='col-md-12'>
						<?=$content;?>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</body>
</html>
