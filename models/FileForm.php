<?php
class FileForm extends CFormModel {
	public $id;
	public $title;
	public $info;

	public function rules(){
		return array(
			array('id','required',),
			array('title, info','safe'),
		);
	}
	public function attributeLabels()
	{
		return array(
			'title'=>'Titulo Principal',
			'info'=>'Informacion Adicional',
		);
	}
	
}
