<?php
$url = CHtml::normalizeUrl(array('admin/createcol'));
if(!count($dataProvider->rawData)){
	echo "<p>No hay colecciones aun.</p>"
		."<a class='btn btn-lg btn-primary' href='$url'>Crea una Colección</a>";
	return;
}
echo "<a class='btn btn-lg btn-primary' href='$url'>Crea una Colección</a>";
$this->widget('zii.widgets.CListView', array(
	'id'=>'listcol-grid',
	'dataProvider'=>$dataProvider,
	'itemView'=>'_collection-item',
	)
);
?>

