<?php
	$url = CHtml::normalizeUrl(array('/cms/admin/updatecol','id'=>$data['id']));
	$urlr = CHtml::normalizeUrl(array('/cms/admin/confirmdelete',
			'ok'=>serialize(array('/cms/admin/deletecol','id'=>$data['id'])),
			'cancel'=>serialize(array('/cms/admin/listcol')),
		));
	echo "
		<div class='col-md-12 listrow'>
			<div class='col-md-8'>
				<a href='$url'><span class='fa fa-pencil'></span>
					<span class='font'>{$data['title']}</span></a>
			</div>
			<div class='col-md-4'>
				<a href='$urlr' class='right'><span class='fa fa-remove'></span></a>
			</div>
		</div>";
?>
